import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-register-m',
  templateUrl: './register-m.component.html',
  styleUrls: ['./register-m.component.scss']
})
export class RegisterMComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  onRegisterSubmit(form){
    console.log(form);
  }

}
