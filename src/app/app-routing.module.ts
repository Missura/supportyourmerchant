import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './components/public/home/home.component';
import { LoginComponent } from './components/public/login/login.component';
import { RegisterMComponent } from './components/merchants/register-m/register-m.component';
import { AngularFireAuthGuard, customClaims } from '@angular/fire/auth-guard';
import { DashboardComponent } from './components/merchants/dashboard/dashboard.component';
import { pipe } from 'rxjs';
import { map } from 'rxjs/operators';
import { AddLocationComponent } from './components/merchants/add-location/add-location.component';
import { RegisterComponent } from './components/public/register/register.component';



// authGuardPipes //

//demo (no effect)  
const isAdmin = () => pipe(customClaims ,map(claims => {
  if( claims.admin){
    return true; // ok, go to your route
  }else if(claims.user){
    return ['/whereUsersAreRedirected']; // users with claim user are redirected
  }else{
    return [''];
  }
}));


const routes: Routes = [
  { path: '', component: HomeComponent },

  { path: 'yourstore', component: RegisterMComponent },
  { path: 'login', component: LoginComponent },
  { path: 'start', component: RegisterMComponent },

  // protected routes
  { 
  path: 'dashboard',
  component: DashboardComponent,
  canActivate: [AngularFireAuthGuard],
  //data: { authGuardPipe: isAdmin }
  },
  {
  path: 'addLocation',
  component: AddLocationComponent,
  canActivate: [AngularFireAuthGuard],

  },

  { path: '**', redirectTo: '' }
];



@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
