import { Injectable, NgZone } from '@angular/core';
import { AngularFireAuth } from "@angular/fire/auth";
import { AngularFirestore, AngularFirestoreDocument } from '@angular/fire/firestore';
import { AngularFireFunctions } from '@angular/fire/functions';
import { Router } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import * as firebase from 'firebase/app';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  userData: any; // Save logged in user data
  userClaims: any; 

  constructor(
    public afs: AngularFirestore,   // Inject Firestore service
    private fns: AngularFireFunctions,
    public afAuth: AngularFireAuth, // Inject Firebase auth service
    public router: Router,  
    public ngZone: NgZone, // NgZone service to remove outside scope warning
    private http: HttpClient,
  ) { 
        /* Saving user data in localstorage when 
        logged in and setting up null when logged out */
        this.afAuth.authState.subscribe(user => {
          if (user) {
              //get idTokenResult to check claims etc
              firebase.auth().currentUser.getIdTokenResult()
              .then((idTokenResult) => {
                  this.userClaims = idTokenResult.claims;
              })
              .catch((error) => {
                  //console.log(error);
                  window.alert("leider ist ein Fehler aufgetreten.")
              });
              this.userData = user;
              localStorage.setItem('user', JSON.stringify(this.userData));
              //JSON.parse(localStorage.getItem('user'));
              //this.router.navigate(['/dashboard']);
          } else {
              localStorage.setItem('user', null);
              JSON.parse(localStorage.getItem('user'));
              localStorage.setItem('currentProjectID', null);
              //this.router.navigate(['/login']);
              }
          })
   }

      // login in with email/password
      login(email, password) {
      return this.afAuth.auth.signInWithEmailAndPassword(email, password)
      .then((result) => {
          this.ngZone.run(() => {

              if(result.user.emailVerified){
                  this.router.navigate(['/dashboard']);
              }else{
                  window.alert(['Bitte Verifizieren Sie zuerst ihren Account.'])
              }
              
          return true
          });

      }).catch((error) => {
          //console.log(error.message);
          //window.alert(error.message)
          return false  
      })

  }


  // register up with email/password
  Register(email, password, userdata) {
    return this.afAuth.auth.createUserWithEmailAndPassword(email, password)
    .then((result) => {
        /* Call the SendVerificaitonMail() function when new user sign 
        up  */

        this.SendVerificationMail();
      
        return true;
    }).catch((error) => {
        //window.alert(error.message)
        return false;
    })
  }

  // Send email verfificaiton when new user sign up
  SendVerificationMail() {
    return this.afAuth.auth.currentUser.sendEmailVerification()
    .then(() => {
        this.router.navigate(['verify-email-address']);
    })
  }


  // Reset Forggot password
  ForgotPassword(passwordResetEmail) {
    return this.afAuth.auth.sendPasswordResetEmail(passwordResetEmail)
    .then(() => {
        return true
    }).catch((error) => {
        //console.log(error);
        return false
    })
  }

  // Returns true when user is looged in and email is verified
  get isLoggedIn(): boolean {
    const user = JSON.parse(localStorage.getItem('user'));
    return (user !== null && user.emailVerified !== false) ? true : false;
  }

  // Sign out 
  SignOut() {
    return this.afAuth.auth.signOut().then(() => {
        localStorage.removeItem('user');
        this.router.navigate(['/']);
    })
  }

}
